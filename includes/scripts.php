<?php
/**
 * Styles and scripts.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_enqueue_scripts' ) ) {
	/**
	 * Enqueues styles and scripts.
	 *
	 * @return void
	 */
	function serge_enqueue_scripts() {
		global $serge;

		// Styles
		wp_enqueue_style( $serge['theme']['alias'] . '-fonts', '//fonts.googleapis.com/css?family=Montserrat:400,700|Merriweather:400,300,300italic,400italic,700,700italic,900,900italic', array(), $serge['theme']['version'], 'all' );

		wp_enqueue_style( $serge['theme']['alias'] . '-style', get_stylesheet_uri(), array(), $serge['theme']['version'], 'all' );

		// Scripts
		wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', array(), '2.1.4', false );

		wp_enqueue_script( $serge['theme']['alias'] . '-script', get_template_directory_uri() . '/assets/js/script.min.js', array(), $serge['theme']['version'], true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'serge_enqueue_scripts' );
