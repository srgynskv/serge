<?php
/**
 * Setup.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function serge_setup() {
		/**
		 * Localization.
		 *
		 * 1. /wp-content/languages/themes/serge-ru_RU.mo
		 * 2. /wp-content/themes/child-theme/languages/ru_RU.mo
		 * 3. /wp-content/themes/serge/languages/ru_RU.mo
		 */
		load_theme_textdomain( 'serge', trailingslashit( WP_LANG_DIR ) . 'themes/' );
		load_theme_textdomain( 'serge', get_stylesheet_directory() . '/languages' );
		load_theme_textdomain( 'serge', get_template_directory() . '/languages' );

		/**
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'widgets',
		) );

		/**
		 * Enable support for Post Formats.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array() );
	}
}
add_action( 'after_setup_theme', 'serge_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function serge_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'serge_content_width', 640 );
}
add_action( 'after_setup_theme', 'serge_content_width', 0 );
