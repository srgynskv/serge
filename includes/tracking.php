<?php
/**
 * Tracking codes.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_google_analytics' ) ) {
	/**
	 * Adds Google Analytics tracking code.
	 *
	 * @return void
	 */
	function serge_google_analytics() {
		if ( ! IS_LOCALHOST ) {
			?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70145306-1', 'auto');
  ga('send', 'pageview');

</script>
			<?php
		}
	}
}
add_action( 'wp_head', 'serge_google_analytics', 999 );

if ( ! function_exists( 'serge_yandex_metrika' ) ) {
	/**
	 * Adds Yandex.Metrika tracking code.
	 *
	 * @return void
	 */
	function serge_yandex_metrika() {
		if ( ! IS_LOCALHOST ) {
			?>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter33586674 = new Ya.Metrika({ id:33586674, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/33586674" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
			<?php
		}
	}
}
add_action( 'wp_footer', 'serge_yandex_metrika', 999 );
