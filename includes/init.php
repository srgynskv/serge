<?php
/**
 * Theme initialization.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

// Theme info.
require get_template_directory() . '/includes/theme-info.php';

// Cleanup.
require get_template_directory() . '/includes/cleanup.php';

// Setup.
require get_template_directory() . '/includes/setup.php';

// Nav menus.
require get_template_directory() . '/includes/nav-menus.php';

// Sidebars.
require get_template_directory() . '/includes/sidebars.php';

// Styles and scripts.
require get_template_directory() . '/includes/scripts.php';

// Tracking.
require get_template_directory() . '/includes/tracking.php';

// Custom template tags.
require get_template_directory() . '/includes/template-tags.php';

// Registration errors.
require get_template_directory() . '/includes/registration-errors.php';

// Custom extra functions.
require get_template_directory() . '/includes/extras.php';

// Jetpack compatibility file.
require get_template_directory() . '/includes/jetpack.php';
