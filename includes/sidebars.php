<?php
/**
 * Sidebars.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_register_sidebars' ) ) {
	/**
	 * Registers sidebars.
	 *
	 * @return void
	 */
	function serge_register_sidebars() {
		register_sidebar( array(
			'name' => esc_html__( 'Sidebar Widgets', 'serge' ),
			'id' => 'sidebar-widgets',
			'description' => esc_html__( 'Drag your widgets to this container.', 'serge' ),
			'class' => 'serge',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>',
		) );
	}
}
add_action( 'widgets_init', 'serge_register_sidebars' );
