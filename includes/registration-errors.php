<?php
/**
 * Registration errors.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_illegal_user_logins' ) ) {
	/**
	 * Returns the list of blacklisted usernames.
	 *
	 * @return array Array of blacklisted usernames.
	 */
	function serge_illegal_user_logins() {
		return array(
			'about', 'access', 'account', 'accounts', 'activate', 'activities', 'activity', 'ad', 'add', 'address', 'adm', 'admin', 'administration', 'administrator', 'ads', 'adult', 'advertising', 'affiliate', 'affiliates', 'ajax', 'all', 'alpha', 'analysis', 'analytics', 'android', 'anon', 'anonymous', 'api', 'app', 'apps', 'archive', 'archives', 'article', 'asct', 'asset', 'atom', 'auth', 'authentication', 'avatar',

			'backup', 'bak', 'balancer-manager', 'banner', 'banners', 'beta', 'billing', 'bin', 'blog', 'blogs', 'board', 'book', 'bookmark', 'bot', 'bots', 'bug', 'business',

			'cache', 'caching', 'cadastro', 'calendar', 'call', 'campaign', 'cancel', 'captcha', 'career', 'careers', 'cart', 'categories', 'category', 'cgi', 'cgi-bin', 'changelog', 'chat', 'check', 'checking', 'checkout', 'client', 'cliente', 'clients', 'code', 'codereview', 'comercial', 'comment', 'comments', 'communities', 'community', 'company', 'compare', 'compras', 'config', 'configuration', 'conn', 'connect', 'contact', 'contacts', 'contact-us', 'contact_us', 'contactus', 'contest', 'contribute', 'corp', 'create', 'css',

			'dashboard', 'data', 'db', 'default', 'delete', 'demo', 'design', 'designer', 'destroy', 'dev', 'devel', 'developer', 'developers', 'diagram', 'diary', 'dict', 'dictionary', 'die', 'dir', 'direct_messages', 'directory', 'dist', 'doc', 'docs', 'documentation', 'domain', 'download', 'downloads',

			'ecommerce', 'edit', 'editor', 'edu', 'education', 'email', 'employment', 'empty', 'end', 'enterprise', 'entries', 'entry', 'error', 'errors', 'eval', 'event', 'exit', 'explore',

			'facebook', 'faq', 'favorite', 'favorites', 'feature', 'features', 'feed', 'feedback', 'feeds', 'file', 'files', 'first', 'flash', 'fleet', 'fleets', 'flog', 'follow', 'followers', 'following', 'forgot', 'form', 'forum', 'forums', 'founder', 'free', 'friend', 'friends', 'ftp',

			'gadget', 'gadgets', 'game', 'games', 'get', 'gift', 'gifts', 'gist', 'github', 'graph', 'group', 'groups', 'grunt', 'gruntfile', 'guest', 'guests', 'gulp', 'gulpfile',

			'help', 'home', 'homepage', 'host', 'hosting', 'hostmaster', 'hostname', 'howto', 'hpg', 'html', 'http', 'httpd', 'https',

			'i', 'iamges', 'icon', 'icons', 'id', 'idea', 'ideas', 'image', 'images', 'imap', 'img', 'index', 'indice', 'info', 'information', 'inquiry', 'intranet', 'invitations', 'invite', 'ipad', 'iphone', 'irc', 'is', 'issue', 'issues', 'it', 'item', 'items',

			'java', 'javascript', 'job', 'jobs', 'join', 'js', 'json', 'jump',

			'knowledgebase', 'knowledge-base', 'knowledge_base',

			'lang', 'language', 'languages', 'last', 'ldap-status', 'legal', 'license', 'link', 'links', 'linux', 'list', 'lists', 'log', 'log-in', 'log-out', 'log_in', 'log_out', 'login', 'logout', 'logs',

			'm', 'mac', 'mail', 'mail1', 'mail2', 'mail3', 'mail4', 'mail5', 'mailer', 'mailing', 'maintenance', 'manager', 'manual', 'map', 'maps', 'marketing', 'master', 'me', 'media', 'member', 'members', 'message', 'messages', 'messenger', 'microblog', 'microblogs', 'mine', 'mis', 'mob', 'mobile', 'movie', 'movies', 'mp3', 'msg', 'msn', 'music', 'musicas', 'mx', 'my', 'mysql', 'mysqli',

			'name', 'named', 'nan', 'nav', 'navi', 'navigation', 'net', 'network', 'new', 'news', 'newsletter', 'nick', 'nickname', 'nil', 'notes', 'noticias', 'notification', 'notifications', 'notify', 'ns', 'ns1', 'ns10', 'ns2', 'ns3', 'ns4', 'ns5', 'ns6', 'ns7', 'ns8', 'ns9', 'null',

			'oauth', 'oauth_clients', 'offer', 'offers', 'official', 'old', 'online', 'openid', 'operator', 'order', 'orders', 'organization', 'organizations', 'overview', 'owner', 'owners',

			'page', 'pager', 'pages', 'panel', 'password', 'payment', 'perl', 'phone', 'photo', 'photoalbum', 'photos', 'php', 'pic', 'pics', 'ping', 'plan', 'plans', 'plugin', 'plugins', 'policy', 'pop', 'pop3', 'popular', 'porn', 'portal', 'post', 'postfix', 'postmaster', 'posts', 'pr', 'premium', 'press', 'price', 'pricing', 'privacy', 'privacy-policy', 'privacy_policy', 'privacypolicy', 'private', 'product', 'products', 'profile', 'project', 'projects', 'promo', 'pub', 'public', 'purpose', 'put', 'python',

			'query',

			'random', 'ranking', 'read', 'readme', 'recent', 'recruit', 'recruitment', 'register', 'registration', 'release', 'remove', 'replies', 'report', 'reports', 'repositories', 'repository', 'req', 'request', 'requests', 'reset', 'roc', 'root', 'rss', 'ruby', 'rule',

			'sag', 'sale', 'sales', 'sample', 'samples', 'save', 'school', 'script', 'scripts', 'search', 'secure', 'security', 'self', 'send', 'server', 'server-info', 'server-status', 'service', 'services', 'session', 'sessions', 'setting', 'settings', 'setup', 'share', 'shop', 'show', 'sign-in', 'sign-up', 'sign_in', 'sign_up', 'signin', 'signout', 'signup', 'site', 'sitemap', 'sites', 'smartphone', 'smtp', 'soporte', 'source', 'spec', 'special', 'sql', 'src', 'ssh', 'ssl', 'ssladmin', 'ssladministrator', 'sslwebmaster', 'staff', 'stage', 'staging', 'start', 'stat', 'state', 'static', 'stats', 'status', 'store', 'stores', 'stories', 'style', 'styleguide', 'stylesheet', 'stylesheets', 'subdomain', 'subscribe', 'subscriptions', 'suporte', 'support', 'svn', 'swf', 'sys', 'sysadmin', 'sysadministrator', 'system',

			'tablet', 'tablets', 'tag', 'talk', 'task', 'tasks', 'team', 'teams', 'tech', 'telnet', 'term', 'terms', 'terms-of-service', 'terms_of_service', 'termsofservice', 'test', 'test1', 'test2', 'test3', 'teste', 'testing', 'tests', 'theme', 'themes', 'thread', 'threads', 'tmp', 'todo', 'tool', 'tools', 'top', 'topic', 'topics', 'tos', 'tour', 'translations', 'trends', 'tutorial', 'tux', 'tv', 'twitter',

			'undef', 'unfollow', 'unsubscribe', 'update', 'upload', 'uploads', 'url', 'usage', 'user', 'username', 'users', 'usuario',

			'vendas', 'vendor', 'ver', 'version', 'video', 'videos', 'visit', 'visitor',

			'watch', 'weather', 'web', 'webmail', 'webmaster', 'website', 'websites', 'welcome', 'widget', 'widgets', 'wiki', 'win', 'windows', 'woocommerce', 'woothemes', 'word', 'work', 'works', 'workshop', 'ww', 'wws', 'www', 'www1', 'www2', 'www3', 'www4', 'www5', 'www6', 'www7', 'wwws', 'wwww',

			'xfn', 'xml', 'xmpp', 'xpg', 'xxx',

			'yaml', 'year', 'yml', 'you', 'yourdomain', 'yourname', 'yoursite', 'yourusername',

			'zero',
		);
	}
}
// Available since WordPress v4.4.0
// add_filter( 'illegal_user_logins', 'serge_illegal_user_logins' );

if ( ! function_exists( 'serge_registration_errors' ) ) {
	/**
	 * Filter the errors encountered when a new user is being registered.
	 *
	 * The filtered WP_Error object may, for example, contain errors for an invalid
	 * or existing username or email address. A WP_Error object should always returned,
	 * but may or may not contain errors.
	 *
	 * If any errors are present in $errors, this will abort the user's registration.
	 *
	 * @since 2.1.0
	 *
	 * @param WP_Error $errors A WP_Error object containing any errors encountered during registration.
	 * @param string   $sanitized_user_login User's username after it has been sanitized.
	 * @param string   $user_email User's email.
	 */
	function serge_registration_errors( $errors, $sanitized_user_login, $user_email ) {
		$illegal_user_logins = serge_illegal_user_logins();

		foreach ( $illegal_user_logins as $blacklisted ) {
			if ( false !== stripos( $sanitized_user_login, $blacklisted ) ) {
				$errors->add( 'illegal_user_login', __( '<strong>ERROR</strong>: Sorry, that username is not allowed.', 'serge' ) );

				return $errors;
			}
		}

		return $errors;
	}
}
add_filter( 'registration_errors', 'serge_registration_errors', 10, 3 );
