<?php
/**
 * Nav menus.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_register_nav_menus' ) ) {
	/**
	 * Registers nav menus.
	 *
	 * @return void
	 */
	function serge_register_nav_menus() {
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'serge' ),
		) );
	}
}
add_action( 'after_setup_theme', 'serge_register_nav_menus' );
