<?php
/**
 * Theme cleanup.
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'serge_deregister_scripts' ) ) {
	/**
	 * Deregisters styles and scripts.
	 *
	 * @return void
	 */
	function serge_deregister_scripts() {
		// Scripts.
		wp_deregister_script( 'jquery' );
	}
}
add_action( 'wp_enqueue_scripts', 'serge_deregister_scripts' );
