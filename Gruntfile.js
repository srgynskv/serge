// Gruntfile
module.exports = function( grunt ) {
  'use strict';

  // Requirements
  require( 'time-grunt' )( grunt );

  require( 'jit-grunt' )( grunt, {
    makepot: 'grunt-wp-i18n',
  });

  // Config
  grunt.initConfig( {
    // info
    pkg: grunt.file.readJSON( 'package.json' ),

    // clean
    clean: {
      dist: {
        src: [
          '<%= pkg.dist %>',
        ],
      },
    },

    // copy
    copy: {
      // dist
      dist: {
        expand: true,
        cwd: '',
        src: [
          '**',
          '!.*',
          '!*.{err,json,log,map,md,xml,zip}',
          '!.*/**',
          '!Gruntfile.js',
          '!LICENSE',
          '!<%= pkg.assets.js %>/**/*.{js,map}',
          '<%= pkg.assets.js %>/**/*.min.js',
          '!<%= pkg.assets.scss %>/**',
          '!bower_components/**',
          '!node_modules/**',
          '!<%= pkg.dist %>/**',
        ],
        dest: '<%= pkg.dist %>',
        flatten: false,
      },
    },

    // sass
    sass: {
      options: {
        includePaths: [
          'bower_components/',
        ],
        outputStyle: 'compressed',
        sourceMap: false,
      },
      theme: {
        files: {
          'style.css': '<%= pkg.assets.scss %>/style.scss',
        },
      },
    },

    // postcss
    postcss: {
      options: {
        processors: [
          require( 'pixrem' )(),
          require( 'autoprefixer' )( {
            browsers: 'last 2 versions',
          } ),
          require( 'cssnano' )(),
        ],
        map: false,
      },
      theme: {
        src: 'style.css',
      },
    },

    // concat
    concat: {
      options: {
        // separator: ';',
      },
      // theme: {
      //   src: [],
      //   dest: '',
      // },
    },

    // uglify
    uglify: {
      options: {
        preserveComments: false,
        banner: '/*! <%= pkg.title %> v<%= pkg.version %> - <%= grunt.template.today( "yyyy-mm-dd HH:MM:ss Z" ) %> */\n',
      },

      // theme
      theme: {
        files: {
          '<%= pkg.assets.js %>/script.min.js': [
            '<%= pkg.assets.js %>/navigation.js',
            '<%= pkg.assets.js %>/skip-link-focus-fix.js',
            '<%= pkg.assets.js %>/custom.js',
          ],
        },
      },
    },

    // checktextdomain
    checktextdomain: {
      options: {
        text_domain: '<%= pkg.name %>',
        keywords: [
          '__:1,2d',
          '_e:1,2d',
          '_x:1,2c,3d',
          'esc_html__:1,2d',
          'esc_html_e:1,2d',
          'esc_html_x:1,2c,3d',
          'esc_attr__:1,2d',
          'esc_attr_e:1,2d',
          'esc_attr_x:1,2c,3d',
          '_ex:1,2c,3d',
          '_n:1,2,4d',
          '_nx:1,2,4c,5d',
          '_n_noop:1,2,3d',
          '_nx_noop:1,2,3c,4d',
        ],
      },
      theme: {
        src: [
          '**/*.php',
          '!.*/**',
          '!<%= pkg.assets.dir %>/**',
          '!bower_components/**',
          '!<%= pkg.dist %>/**',
          '!languages/**',
          '!node_modules/**',
          '!**/.*',
        ],
        expand: true,
      },
    },

    // makepot
    makepot: {
      theme: {
        options: {
          cwd: '',
          domainPath: '/languages',
          exclude: [
            '<%= pkg.assets.dir %>/*',
            'bower_components/*',
            'node_modules/*',
            '<%= pkg.dist %>/*',
          ],
          mainFile: 'style.css',
          potComments: 'Copyright (c) <%= grunt.template.today( "yyyy" ) %> <%= pkg.author.name %>',
          potHeaders: {
            poedit: true,
            'x-poedit-keywordslist': true,
            'report-msgid-bugs-to': '',
            'language-team': 'LANGUAGE <EMAIL@ADDRESS>',
          },
          type: 'wp-theme',
          updateTimestamp: true,
          updatePoFiles: false,
        },
      },
    },

    // potomo
    potomo: {
      theme: {
        files: [ {
          expand: true,
          cwd: 'languages/',
          src: '*.po',
          dest: 'languages/',
          ext: '.mo',
          nonull: true,
        } ],
      },
    },

    // watch
    watch: {
      options: {
        spawn: false,
        livereload: true,
      },

      // Gruntfile
      gruntfile: {
        files: [
          'Gruntfile.js',
        ],
        tasks: [
          'build',
        ],
      },

      // styles
      styles: {
        files: [
          '<%= pkg.assets.scss %>/**/*.scss',
        ],
        tasks: [
          'sass',
          'postcss',
        ],
      },

      // scripts
      scripts: {
        files: [
          '<%= pkg.assets.js %>/**/*.js',
          '!<%= pkg.assets.js %>/**/*.min.js',
        ],
        tasks: [
          'concat',
          'uglify',
        ],
      },

      // templates
      templates: {
        files: [
          '**/*.php',
          '!{<%= pkg.assets.dir %>,bower_components,languages,node_modules,<%= pkg.dist %>}/**',
        ],
      },
    },
  } );

  // Tasks
  grunt.registerTask( 'install', [
    'build',
  ] );

  grunt.registerTask( 'default', [
    'watch',
  ] );

  grunt.registerTask( 'build', [
    'clean',
    'sass',
    'postcss',
    'concat',
    'uglify',
    'copy:dist',
  ] );

  grunt.registerTask( 'localize', [
    'checktextdomain',
    'makepot',
    // 'potomo',
  ] );
};
