<?php
/**
 * Serge functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Serge
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

// Initialization.
require get_template_directory() . '/includes/init.php';
